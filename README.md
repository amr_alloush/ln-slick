# ln-slick

* This is a fork from slick/slick to add support for scala 12 on slick 3.0.3

* This repo contains **only** slick 3.0.3 barnch

* Two test cases in `slick-testkit/src/main/scala/com/typesafe/slick/testkit/tests/RelationalMapperTest.scala` were commented because of errors after scala 2.12 upgrade
